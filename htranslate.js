function parseHeb(verse) {
	// https://raw.githack.com/openscriptures/morphhb/master/parsing/HebrewMorphologyCodes.html
	log(verse);
	var osisID = verse.attributes.getNamedItem("osisID").value;
	var ref = [...osisID.matchAll(/^(.*?)\.(.*?)\.(.*?)$/g)][0];
	let phrase = [];
	// let words = Array.from(verse.childNodes).filter(node => /w|seg/i.test(node.nodeName));
	let words = verse.querySelectorAll("verse > w, verse > seg");
	let vsuffix = '';
	words.forEach((w, i) => {
		if (/^w$/.test(w.nodeName)) {
			let prelemma = w.attributes.getNamedItem("lemma").value;
			let lemma = prelemma.split("/");
			let premorph = w.attributes.getNamedItem("morph").value;
			let morph = premorph.replace(/^(H|A)/, '').split("/").map(j => [...j]);
			// if (/\+$/.test(prelemma)) {
			// 	return;
			// }
			if (i !== 0) {
				if (words[i - 1].attributes.getNamedItem("morph") !== null) {
					if (premorph.indexOf("N") != -1 && words[i - 1].attributes.getNamedItem("morph").value.indexOf("V") != -1 && !premorph.match(`C`)) {
						phrase.push(", <i>that is</i>,");
					}
				}
			}
			morph.forEach((m, j) => { // loop through each given morph, each are arrays of letters
				/*if (/(H|A)/.test(m[0])) {
					m.shift();
				}*/
				try {
					let suffix;
					if (/(N|A)/.test(m[0]) || (/V/.test(m[0]) && m.length > 3)) {
						if (m.at(-1) === "c") {
							suffix = "of";
						} else if (m.at(-1) === "d") {
							phrase.push("the");
						}
						if (m[3] === 's' && !lemma.includes('d')) {
							phrase.push("a");
						}
						if (/(a|c|d)/.test(m.at(-1))) {
							m.pop();
						}
					}
					if (/A/.test(m[0]) && /a/.test(m[1])) {
						m.splice(3, 1); // remove Number
					}
					log(m[0], j, lemma[j], lemma.length, morph.length);
					if (/^(l|b|k|m|d|s)$/.test(lemma[j])) { // Prefixes
						switch (lemma[j]) {
							case "l": phrase.push("for"); break;
							case "b": phrase.push("in"); break;
							case "k": phrase.push("as"); break;
							case "m": phrase.push("from"); break;
							case "d": phrase.push("the"); break;
							case "s": phrase.push("what"); break;
							default: console.warn("Strange prefix found:", lemma[j]);
						}
					} else if (/C|R/.test(m[0]) && j === 0 && (lemma[j] === "c" || lemma.length !== morph.length)) { // Conjunction
						phrase.push("and");
					} else if (m[0] === "S") { // Suffix
						switch (m[1]) {
							// only handling case "p", but catch others and do nothing
							case "d": break;
							case "h": break;
							case "n": break;
							case "p":
								let n = m[4];
								let nwm = words[i+1].attributes.getNamedItem('morph');
								let nwmp = nwm ? Boolean(nwm.value.match(`V.(r|s)`)) : false; // next word's morph is verb participle
								let pm = morph[j-1].slice(-1)[0];
								let pmc = (Object.prototype.toString.call(pm) === "[object Array]" ? pm[0].slice(-1) : pm) === 'c' || (Object.prototype.toString.call(phrase.at(-1)) === "[object Array]" ? phrase.at(-1)[0].slice(-1) : phrase.at(-1)).match(`( |^)of$`); // previous morph is construct
								// log(m, nwm, nwmp, pm, pmc, [...phrase]);
								switch (m[2]) { // person
									case "1": phrase.push(n === "s" ? (pmc ? 'mine' : (nwmp ? "I am" : "me")) : (pmc ? 'ours' : (nwmp ? "we are" : "us"))); break;
									case "2": phrase.push(n === "s" ? (pmc ? 'yours' : (nwmp ? "you are" : "you")) : (pmc ? "y'alls" : (nwmp ? "y'all are" : "y'all"))); break;
									case "3":
										switch (m[3]) { // gender
											case "m": phrase.push(n === "s" ? (pmc ? 'his' : (nwmp ? "he is" : "him")) : (pmc ? 'theirs' : (nwmp ? "they are" : "them"))); break;
											case "f": phrase.push(n === "s" ? (pmc ? 'hers' : (nwmp ? "she is" : "her")) : (pmc ? 'theirs' : (nwmp ? "they are" : "them"))); break;
											case "c": phrase.push(n === "s" ? (pmc ? 'its' : (nwmp ? "it is" : "it")) : (pmc ? 'theirs' : (nwmp ? "they are" : "them"))); break;
											default: console.warn("Wierd gender!", m[3], lemma[j]);
										}
										break;
								}
								break;
						}
					} else if (m[0] === "T" && m[1] === "i" && lemma[j] === "i") {
						vsuffix += '?';
					} else {
						let k = j;
						if (lemma.length === 1) {
							k = 0;
						} else if (morph.length !== lemma.length && morph[0] === 'C' && !lemma.some(s => /c/.test(s))) {
							k = j - 1;
						}
						let l = lemma[k];
						log(lemma, k, l, w);
						let def = hstrong['H'+l.replace(/(\s[a-z])$/, "").replace('+', '')];
						let forms = def.forms;
						let smorph = premorph.split('/')[j];
						if (morph.some(s => s.some(t => /S/.test(t)))) { // Suffix
							let sm = morph[j + 1];
							// log("L1:", smorph, sm, morph, m);
							switch (sm[1]) {
								case "d": phrase.push("to"); break;
								case "h": phrase.push("really"); break;
								case "n": break;
								case "p": break;
							}
						}
						function push(form) {
							phrase.push([form !== "" ? form : `<u>${def.xlit}</u>`, l, smorph, def.derivation, def.strongs_def]);
						}
						switch (m.length) {
							case 1: push(forms?.[m[0]]); break;
							case 2: push(forms?.[m[0]]?.[m[1]]); break;
							case 3: push(forms?.[m[0]]?.[m[1]]?.[m[2]]); break;
							case 4: push(forms?.[m[0]]?.[m[1]]?.[m[2]]?.[m[3]]); break;
							case 5: push(forms?.[m[0]]?.[m[1]]?.[m[2]]?.[m[3]]?.[m[4]]); break;
							case 6: push(forms?.[m[0]]?.[m[1]]?.[m[2]]?.[m[3]]?.[m[4]]?.[m[5]]); break;
							case 7: push(forms?.[m[0]]?.[m[1]]?.[m[2]]?.[m[3]]?.[m[4]]?.[m[5]]?.[m[6]]); break;
							default: console.warn("Word form is too long or too short:", lemma, m, m.length, w);
						}
					}
					if (suffix) {
			            phrase.push(suffix);
		            }
				} catch (e) {
					console.warn(e, lemma, lemma[j], m, m.length, phrase, w);
				}
			});
		} else {
			let type = w.attributes.getNamedItem("type").value;
			switch (true) {
				case /x-sof-pasuq/i.test(type): phrase.push(vsuffix != '' ? vsuffix : "."); break; // period
				case /x-maqqef/i.test(type): phrase.push("-"); break; // hyphen
				case /x-paseq/i.test(type): break;
				case /x-pe/i.test(type): phrase.push('<br>'); break;
				case /x-samekh/i.test(type): phrase.push('<br>'); break;
				case /x-reversednun/i.test(type): phrase.push('[Inverted nun]'); break;
				case /x-large/i.test(type): break;
				default: console.warn(type+" found!", w);
			}
		}
	});
	var joined = phrase
		.map(j => Object.prototype.toString.call(j) === "[object Array]" ? `<a href="/hebrew/?q=${j[1].replace(/(\s*[a-z\+])$/, '')}" title="${j[1]}: ${j[2]} | ${j[3]} | ${j[4]}">${j[0] === "" ? "###" : Object.prototype.toString.call(j[0]) === "[object Object]" ? JSON.stringify(j[0]) : j[0]}</a>` : j) // convert words to links, while fixing objects being coverted to strings
		.map(j => Object.prototype.toString.call(j) === "[object Object]" ? JSON.stringify(j) : j) // fixed objects being converted to strings
		.join(" ").replace('undefined', '###').replace(/ (, |\.|\?|-) */g, "$1").replace(/^<a href="\/hebrew\/\?q=200(5|9)" title="200(5|9): HTm \| (a primitive particle|prolongation for H2005 \(הֵן\)) \| (lo!; also (as expressing surprise) if|lo!)">— look!<\/a>/, '<a href="/hebrew/\?q=200$1" title="200$2: HTm; $3; $4">look!</a>'); // join each word with " ", then fix excess spacing around punctuation; remove emdash from H2009 & H2005 when starting a verse
	var cref = `${ref[1]} ${ref[2]}:${ref[3]}`; // verse ref
	// log('pre:', joined);
	[...joined.matchAll(/(?: |-|^)(a) .*?>(.)/gd)].filter(v => /a|e|i|o|u/.test(v[2])).forEach((m, i) => joined = joined.replace(new RegExp(`(.{${m.indices[1][0]+i}})(.)`), '$1an')); // replace a with an where next word begins with vowel. Fixes many a/an issues
	// log('post:', joined);
	let header;
	if (headers?.[osisID]) {
		header = headers[osisID];
	}
	return `${header ? `<h3>${header}</h3>` : ''}<sup><a href="./?q=${cref}">${cref}</a></sup> ` + (joined[0] === '<' ? joined.replace(/^(.*?>)(.)/, '$1'+joined.substr(joined.match(/^.*?>(.)/d).indices[1][0], 1).toUpperCase()) : joined.replace(/^(.)/, joined.substr(0, 1).toUpperCase())).replace(/(, <i>that is<\/i>,| )a <a href="\/hebrew\/\?q=5002" title="5002: HNcmsc \| from H5001 \(נָאַם\) \| an oracle">whisper<\/a> of-<a href="\/hebrew\/\?q=3068" title="3068: HNp \| from H1961 \(הָיָה\); \(the\) self-Existent or Eternal \| Jehovah, Jewish national name of God">YHVH \(The-One-Who-Is\)<\/a>( |\.)/gm, ' — a <a href="/hebrew/?q=5002" title="5002: HNcmsc | from H5001 (נָאַם) | an oracle">whisper</a> of-<a href="/hebrew/?q=3068" title="3068: HNp | from H1961 (הָיָה); (the) self-Existent or Eternal | Jehovah, Jewish national name of God">YHVH (The-One-Who-Is)</a> —$2').replace(' —.', '.'); // add ref link to beginning of verse, captalize first letter of verse
}
