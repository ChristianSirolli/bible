---
layout: post
title: "40 Years After"
---
Jesus drank the cup of God's anger instead of Israel so they could repent. They were given 40 years to repent and turn to Him, but they did not so they were forced to drink the cup of God's anger anyways. So 40 years after Jesus died, the Romans destroyed Jerusalem and the Temple.

The Romans came and swept through the land of Israel like a flood, crushing all of Israel. This war lasted 7 years. In the middle of the 7 years, they laid seige to Jerusalem and destroyed it and the Temple, not leaving one stone of the Temple on top of another. Now the Roman Empire was an empire like none that came before it. The 10th emperor began to reign in middle of the 7-year war, and continued the war for the last 3 1/2 years.
