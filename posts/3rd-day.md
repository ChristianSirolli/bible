---
layout: post
title: "3rd Day"
---
Ref | Verse
---|---
Mt 12:40 | For as Jonah was three days and three nights in the belly of a huge fish, so the Son of Man will be three days and three nights in the heart of the earth.
Mt 16:21 | From that time on Jesus began to explain to his disciples that he must go to Jerusalem and suffer many things at the hands of the elders, the chief priests and the teachers of the law, and that he must be killed and on the third day be raised to life.
Mt 17:23 | They will kill him, and on the third day he will be raised to life.” And the disciples were filled with grief.
Mt 20:19 | and will hand him over to the Gentiles to be mocked and flogged and crucified. On the third day he will be raised to life!”
Mt 26:61 | and declared, “This fellow said, ‘I am able to destroy the temple of God and rebuild it in three days.’”
Mt 27:40 | and saying, “You who are going to destroy the temple and build it in three days, save yourself! Come down from the cross, if you are the Son of God!”
Mt 27:63 | “Sir,” they said, “we remember that while he was still alive that deceiver said, ‘After three days I will rise again.’
Mt 27:64 | So give the order for the tomb to be made secure until the third day. Otherwise, his disciples may come and steal the body and tell the people that he has been raised from the dead. This last deception will be worse than the first.”
Mk 8:31 | He then began to teach them that the Son of Man must suffer many things and be rejected by the elders, the chief priests and the teachers of the law, and that he must be killed and after three days rise again.
Mk 9:31 | because he was teaching his disciples. He said to them, “The Son of Man is going to be delivered into the hands of men. They will kill him, and after three days he will rise.”
Mk 10:34 | who will mock him and spit on him, flog him and kill him. Three days later he will rise.”
Mk 14:58 | “We heard him say, ‘I will destroy this temple made with human hands and in three days will build another, not made with hands.’”
Mk 15:29 | Those who passed by hurled insults at him, shaking their heads and saying, “So! You who are going to destroy the temple and build it in three days,
Lk 9:22 | And he said, “The Son of Man must suffer many things and be rejected by the elders, the chief priests and the teachers of the law, and he must be killed and on the third day be raised to life.”
Lk 18:33 | they will flog him and kill him. On the third day he will rise again.”
Lk 24:7 | ‘The Son of Man must be delivered over to the hands of sinners, be crucified and on the third day be raised again.’ ”
Lk 24:21 | but we had hoped that he was the one who was going to redeem Israel. And what is more, it is the third day since all this took place.
Lk 24:46 | He told them, “This is what is written: The Messiah will suffer and rise from the dead on the third day,
Jn 2:19 | Jesus answered them, “Destroy this temple, and I will raise it again in three days.”
Acts 10:40 | but God raised him from the dead on the third day and caused him to be seen.
1 Co 15:4 | that he was buried, that he was raised on the third day according to the Scriptures,
