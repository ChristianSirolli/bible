---
layout: post
title: Family Tree
---
<pre>
		     Adam - Eve
		       /  |   \
		   Cain  Seth  Abel
			 ...
			 Enoch
			 ...
			 Noah
			/  |  \
		Japheth  Shem  Ham
		         ...
			Eber
			 ...
			Terah
		---------------------
	       /	     |       \
Hagar - Abraham - Sarah	   Nahor     Haran
      |         |	   /   \	|
   Ishmael    Isaac - Rebekah   Laban  Lot
		   / \		 /  \
		Esau  Jacob - Leah - Rachel
			|         [Jacob's wives and their children]
                  ---------------------------------------------------------------------
		Leah				     Rachel	      Bilhah	    Zilpah
Reuben Simeon Levi Judah Issachar Zebulun Dinah   Joseph Benjamin   Dan Naphtali   Gad Asher
		|    |					    |
	       ...   |					   ...
	      Moses ...					    |
		     |					   Saul
		   David
		    ...
		   Jesus
</pre>
