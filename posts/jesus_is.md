---
layout: post
title: Jesus Is
---
Jesus is the new Adam, the One who crushes the snake. (Genesis 3:15)

Jesus is the new Noah, the One who brings comfort. (Genesis 5:29)

Jesus is the new Abraham, the One who brings blessing to all nations. (Genesis 12:2)

Jesus is the new Moses, the One who intercedes with the Father on our behalf. (Deuteronomy 18:15-18)

Jesus is the new David, the One who is king of Israel. (Isaiah 11)

Jesus is the new Solomon, the One who builds a temple for God and reigns forever. (2 Samuel 7:12-16, Isaiah 9:7, John 2:19)
