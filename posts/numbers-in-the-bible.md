---
layout: post
title: Numbers in the Bible
---

In the Bible, numbers have a symbolic significance. We can learn from passages where these numbers are repeated to learn their meaning, so we can infer the meaning of passages where they appear elsewhere.

 * **3.** 
 * **5.** This number is said to be the number of grace. When Moses intercedes before God on the Israelites' behalf, he gives 5 reasons why God should relent from destroying them.
 * **7.** This number is related to an idea of completion.
 * **10.** God often speaks in sets of 10. In the creation narrative of [Genesis 1](/genesis/1) and part of [Genesis 2](/genesis/2), the phrase "God said" is repeated 10 times. Also, there are 10 plagues that come against Egypt and there are 10 commandments.
 * **12.** This is the number of the tribes of Israel. 
 * **666.** This is popularly associated with the mark of the beast. Less known is the association with Solomon.
