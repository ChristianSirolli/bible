---
layout: post
title: Bible Reading Plan
---
Here is a plan for reading the Bible that is recommended to be done in a group setting.

Week | Passage | Topic | Psalm
---|---|---|---
1 | Gen 1-5 | Origins | Psalm 1
2 | Gen 6-11 | The Flood | Psalm 2
3 | Gen 12-20 | Abraham | Psalm 3
4 | Gen 21-26 | Isaac | Psalm 4
5 | Gen 27-36 | Jacob | Psalm 5
6 | Gen 37-50 | Joseph | Psalm 6
7 | Exo 1- | The Exodus | Psalm 7
