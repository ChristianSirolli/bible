---
layout: post
title: Chronology of Israel's Monarchy
---
<pre>
n = began count of reign with next year of predecessor
p = began count of reign with previous year of predecessor
NC = no coregency possible/likely with successor

/ = began in prefixed year of suffixed ruler
c = coregency with successor, prefixed with length of coregency
+ = reigned for suffixed years after the death of prefixed ruler
sr = sole reign counted, coregency not taken into consideration
cr = coregency counted

Format: Name length of reign|year/ruler|"flags"|info

Creation to split of kingdom = 2989 +/- 10 years
----------Kingdom split----------
I				J
Jeroboam 22|1/Rehoboam		Rehoboam 17|1/Jeroboam|NC
Nadab 2|2/Asa|p|killed		Abijah 3|18/Jeroboam|n|1c
Baasha 24|3/Asa|p|1c		Asa 41|20/Jeroboam|p|NC
Elah 2|26/Asa|killed		Jehoshaphat 25|4/Ahab|n|~10/3c
Zimri 7d|27/Asa|NC		Jehoram 8|5/Joram|didn't count co if 10c
Omri/Tibni 6|25/Asa		Ahaziah 1|11/12/Joram
Omri 6|31/Asa|NC
Ahab 22|38/Asa|killed
Ahaziah 2|17/Jehoshaphat|NC
Joram 12|2/Jehoram|18/Jehoshaphat|p
----------Jehu kills Joram and Ahaziah----------
Jehu 28|1/Athaliah		Athaliah 6|1/Jehu
Jehoahaz 17|23/Joash|n|2c	Joash 40|7/Jehu|n|NC
Jehoash 16|37/Joash|p		Amaziah 29|2/Jehoash|Jehoash+n15|p|NC
Jeroboam 41|15/Amaziah		Azariah 52|27/Jeroboam|4c
Zechariah 6m|38/Azariah|NC	Jotham 16|2/Pekah|sr|12c
Shallum 1m|39/Azariah|n|NC	Ahaz 16|17/Pekah|p
Menahem 10|39/Azariah|p|NC	Hezekiah 29|3/Hoshea|15c
Pekahiah 2|50/Azariah|n		Manasseh 55|14/15/Hezekiah
Pekah 20|52/Azariah		Amon 2|NC
Hoshea 9|cr20/Jotham|12/Ahaz	Josiah 31|NC
	Jehoahaz 3m|exiled to Egypt|NC|p
	Jehoiakim 11|NC|p
	Jehoiachin 3m|exiled to Babylon|NC|p
	Zedekiah 11|8/Nebuchadnezzar|p
	Fall of Jerusalem|19/Nebuchadnezzar
</pre>
