---
layout: post
title: Christmas
---
Here are some possible dates for when Christ was born: (c = Jesus' conception, b = Jesus' birth, j = John the Baptist's birth)
<pre>
8bc
Pesach - Apr 25-May 2
Sukkot - Oct 20-26
Hanukkah - Dec 28-Jan 4 (7bc)
c=Jan 1-4 (7bc)
b=Oct 8-11 (7bc)
j=Jul 1

7bc
Pesach - Apr 13-20
Sukkot - Oct 8-14
Hanukkah - Dec 16-23
c=Dec 21- 23
b=Sep 27-29
j=Jun 21

6bc
Pesach - Apr 2-9
Sukkot - Sep 27-Oct 3
Hanukkah - Dec 6-13
h-s=44 weeks

5bc
Pesach - Apr 21-28
Sukkot - Oct 16-22
Hanukkah - Dec 25-Jan 1 (4bc)
c=Dec 30-Jan 1
b=Oct 6-8
j=Jun 30

4bc
Pesach - Apr 11-18
Sukkot - Oct 6-12
Hanukkah - Dec 14-21
c=Dec 18-21
b=Sep 24-27
j=Jun 18, 25, jul 2

3bc
Pesach - Mar 30-Apr 6
Sukkot - Sep 24-30
Hanukkah - Dec 2-9 
h-s=44 weeks

2bc
Pesach - Apr 18-25
Sukkot - Oct 13-19
Hanukkah - Dec 22-29
c=Dec 27-29
b=Oct 2-4
j=Jun 27

1bc
Pesach - Apr 7-14
Sukkot - Oct 2-8
Hanukkah - Dec 10-17 
h-s=43 weeks and 6 days

1ad
Pesach - Apr 25-May 2
Sukkot - Oct 20-26
Hanukkah - Dec 29-Jan 5 (2ad)

</pre>
