---
layout: post
title: Basic Timeline of the Bible
---
This timeline tracks major events throughout the Bible and brings us to speed on history up to the present.
<pre>
6000ya --- Creation, Adam and Eve
	|- Seth
	|- Enosh
	|- Cainan
	|- Mahalalel
	|- Jared
	|- Enoch
	|- Methuselah
	|- Lamech
5000ya --- Noah
	|- Shem
	|- Great Flood
	|- Eber
	|- Peleg, Tower of Babel (Babylon)
	|- Reu
	|- Serug
	|- Nahor
	|- Terah
4000ya --- Abraham
	|- Isaac
	|- Jacob
	|- 12 Sons of Jacob
	|- Exodus
	|- Wandering in the wilderness
	|- Judges
	|- Saul
	|- David
3000ya --- Solomon, Temple is built
	|- Division of the nation
	|- Many kings
	|- Northern kingdom is exiled
	|- Southern kingdom is exiled for 70 years
	|- Return of exiles, Temple is rebuilt
	|- Alexander the Great
	|- Maccabees
	|- Roman Empire
2000ya --- Jesus
	|- Early Church
	|- Jerusalem and Temple destroyed
	|- Book of Revelation
	|- Christianity becomes official religion of Roman Empire
	|- Fall of Roman Empire
	|- Catholic Church
	|- Beginning of Islam
	|- Crusades
1000ya --- Middle Ages
	|- More Crusades
	|- Renaissance
	|- Christopher Columbus
	|- Protestant Reformation
	|- USA
	|- WWI
	|- WWII
	|- Space race/Cold War
Now    ---
</pre>
