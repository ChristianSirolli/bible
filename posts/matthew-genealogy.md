---
layout: post
title: Jesus' Genealogy in Matthew
---
What is the purpose of the genealogy in Matthew? He starts his account of the gospel out with a genealogy that is broken into 3 segments of 14 generations. It goes from Abraham to David, from David to the exhile, and from the exhile to Jesus. Why does Matthew start with this?

Both Abraham and David were given promises by God (Genesis 12:1-3, 2 Samuel 7:5-16), but the exhile and the time after made it look like God was not being faithful to His promises. (Psalm 89:38-51)

Matthew began his account of the gospel by going through Jesus' genealogy to show that Jesus is the culmination of both promises, and that through Jesus, God is being faithful to His promises.

The numbers in the genealogy are significant: 14 is 7*2; 7 is for the fact that Jesus is the culmination of these promises, and 2 is for the 2 promises. The number 14 is repeated 3 times for some reason I am unsure about.
