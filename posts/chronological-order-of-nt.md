---
layout: post
title: Chronological Order of New Testament Books
---
1. James
2. 1 Thessalonians
3. 2 Thessalonians
4. Galatians
5. 1 Corinthians
6. 2 Corinthians
7. Romans
8. 1 Peter
9. Philippians
10. Matthew
11. Titus
12. Philemon
13. Mark
14. Ephesians
15. Colossians
16. 1 Timothy
17. Hebrews
18. 2 Peter
19. 2 Timothy
20. Luke
21. Acts
22. Jude
23. John
24. 1 John
25. 2 John
26. 3 John
27. Revelation
