function parseGrk(verse) {
	function getGS(num) {
		return gstrong['G'+num];
	}
	// https://raw.githack.com/openscriptures/morphhb/master/parsing/HebrewMorphologyCodes.html
	log(verse);
	var osisID = verse.attributes.getNamedItem("osisID").value;
	var ref = [...osisID.matchAll(/^(.*?)\.(.*?)\.(.*?)$/g)][0];
	let phrase = [];
	// let words = Array.from(verse.childNodes).filter(node => /w|seg/i.test(node.nodeName));
	let words = verse.querySelectorAll("verse > w, verse > seg");
	let vsuffix = '';
	words.forEach((w, i) => {
		if (/^w$/.test(w.nodeName)) {
			let lemma = w.attributes.getNamedItem("lemma").value;
			let morph = w.attributes.getNamedItem("morph").value.split('-');
			let def = getGS(lemma);
			let form = def?.forms?.[morph[0]];
			switch (morph.length) {
				case 1: break; // form does not need traversing
				case 2:
					l2 = morph[1].split('');
					for (i of l2) {
						form = form?.[i];
					}
					break;
				case 3:
					l2 = morph[1].split('');
					for (i of l2) {
						form = form?.[i];
					}
					l3 = morph[2].split('');
					for (i of l3) {
						form = form?.[i];
					}
					break;
				case 4:
					l2 = morph[1].split('');
					for (i of l2) {
						form = form?.[i];
					}
					l3 = morph[2].split('');
					for (i of l3) {
						form = form?.[i];
					}
					l4 = morph[3].split('');
					for (i of l4) {
						form = form?.[i];
					}
					break;
			}
			phrase.push([form ? form : `<u>${def.xlit}</u>`, lemma, morph, def.derivation, def.strongs_def]);
		} else {
			let type = w.attributes.getNamedItem("type").value;
			switch (true) {
				case /x-period/i.test(type): phrase.push(vsuffix != '' ? vsuffix : "."); break; // period
				case /x-comma/i.test(type): phrase.push(","); break; // comma
				case /x-semicolon/i.test(type): phrase.push(";"); break; // semicolon
				case /x-br/i.test(type): phrase.push('<br>'); break; // break
				default: console.warn(type+" found!", w);
			}
		}
	});
	var joined = phrase
		.map(j => Object.prototype.toString.call(j) === "[object Array]" ? `<a href="/greek/?q=${j[1].replace(/(\s*[a-z\+])$/, '')}" title="${j[1]}: ${j[2]} | ${j[3]} | ${j[4]}">${j[0] === "" ? "###" : Object.prototype.toString.call(j[0]) === "[object Object]" ? JSON.stringify(j[0]) : j[0]}</a>` : j) // convert words to links, while fixing objects being coverted to strings
		.map(j => Object.prototype.toString.call(j) === "[object Object]" ? JSON.stringify(j) : j) // fixed objects being converted to strings
		.join(" ").replace('undefined', '###').replace(/ (; |, |\.) */g, "$1"); // join each word with " ", then fix excess spacing around punctuation
	var cref = `${ref[1]} ${ref[2]}:${ref[3]}`; // verse ref
	// log('pre:', joined);
	[...joined.matchAll(/(?: |-|^)(a) .*?>(.)/gd)].filter(v => /a|e|i|o|u/.test(v[2])).forEach((m, i) => joined = joined.replace(new RegExp(`(.{${m.indices[1][0]+i}})(.)`), '$1an')); // replace a with an where next word begins with vowel. Fixes many a/an issues
	// log('post:', joined);
	let header;
	if (headers?.[osisID]) {
		header = headers[osisID];
	}
	return `${header ? `<h3>${header}</h3>` : ''}<sup><a href="./?q=${cref}">${cref}</a></sup> ` + (joined[0] === '<' ? joined.replace(/^(.*?>)(.)/, '$1'+joined.substr(joined.match(/^.*?>(.)/d).indices[1][0], 1).toUpperCase()) : joined.replace(/^(.)/, joined.substr(0, 1).toUpperCase())); // add ref link to beginning of verse, captalize first letter of verse
}
